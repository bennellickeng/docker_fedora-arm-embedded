# Copyright 2017-2022 Bennellick Engineering Limited
# This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; version 2.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

FROM fedora:35

RUN dnf install -y \
  make \
  git \
  bzip2 \
  scons \
  cmake \
  protobuf-compiler \
  python3-protobuf \
  zip \
  patch \
  dtc \
  gperf \
  python3-pip \
  ninja-build \
  gcc \
  gcc-c++ \
  python3-devel \
  xz

RUN set -o pipefail && \
  curl  https://armkeil.blob.core.windows.net/developer/Files/downloads/gnu/11.2-2022.02/binrel/gcc-arm-11.2-2022.02-x86_64-arm-none-eabi.tar.xz | tar -C /opt -xJf - && \
  echo "export PATH=/opt/gcc-arm-11.2-2022.02-x86_64-arm-none-eabi/bin/:$PATH" > /etc/profile.d/arm-none-eabi_path.sh
