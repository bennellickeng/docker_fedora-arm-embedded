
# Fedora ARM Embedded Build Environment

A Fedora based image containing an arm-none-eabi toolchain. Intended for embedded (ARM Cortex M0/M3/M4/M7) development.

The toolchain is installed in `/opt/[toolchain-name]`, where `[toolchain-name]` is dependant on the version of the toolchain installed in the image.

To simplify using the toolchain, the `bin` directory of the toolchain is added to the system `PATH` environment variable by a file in `/etc/profile.d`. These files are only sourced for login shells, so you may have to pass `-l` to your shell when invoking it. Bitbucket Pipelines appears to handle this automatically and toolchain is available on the path in Pipelines.

Copyright 2017-2022 Bennellick Engineering Limited
